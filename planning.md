##feature 1
* [x] Fork and clone the starter project from Django Two-Shot

* [x] Create a new virtual environment in the repository directory for the project

* [x] Activate the virtual environment

* [x] Upgrade pip

* [x] Install django

* [x] Install black

* [x] Install flake8

* [x] Install djhtml

* [x] Install djlint

* [x] Deactivate your virtual environment

* [x] Activate your virtual environment

* [x] Use pip freeze to generate a requirements.txt file

* [x] Create a Django project named "expenses"

* [x] Run the migrate command to update the database for the migrations that come with a default Django installation

* [x] Create a superuser if you want to access the admin
  
* [x] test
---
##feature 2
* [x] Create a Django app named accounts and install it in the expenses Django project in the INSTALLED_APPS list
* [x] Create a Django app named receipts and install it in the expenses Django project in the INSTALLED_APPS list
* [x] Run the migrations
* [x] Create a super user
* [x] test
---
##feature 3
* [x] in receipts create 3 models
  * [x] ExpenseCategory
    * [x] a name property that contains characters with a maximum length of 50 characters
    * [x] an owner property that is a foreign key to the User model with:
      * [x] a related name of "categories"
      * [x] a cascade deletion relation
    * [x] a __str__ method that returns the name value
  * [x] Account
    * [x] a name property that contains characters with a maximum length of 100 characters
    * [x] a number property that contains characters (not numbers) with a maximum length of 20 characters
    * [x] an owner property that is a foreign key to the User model with: 
      * [x] a related name of "accounts
      * [x] a cascade deletion relation
    * [x] a __str__ method that returns the name value 
  * [x] Receipt
    * [x] a vendor property that contains characters with a maximum length of 200 characters
    * [x] a total property that is a DecimalField with:    
      * [x] three decimal places
      * [x] a maximum of 10 digits
    * [x] a tax property that is a DecimalField with: 
      * [x] three decimal places
      * [x] a maximum of 10 digits
    * [x] a date property that contains a date and time of when the transaction took place
    * [x] a purchaser property that is a foreign key to the User model with: 
      * [x] a related name of "receipts"
      * [x] a cascade deletion relation
    * [x] a category property that is a foreign key to the ExpenseCategory model with: 
      * [x] a related name of "receipts"
      * [x] a cascade deletion relation
    * [x] an account property that is a foreign key to the Account model with: 
      * [x] a related name of "receipts"
      * [x] a cascade deletion relation
      * [x] allowed to be null
---
##feature 4
* [x] Register the three models with the admin so that you can see them in the Django admin site.
---
##feature 5
* [x] Create a view that will get all of the instances of the Receipt model and put them in the context for the template.
* [x] Register that view in the receipts app for the path "" and the name "home" in a new file named receipts/urls.py.
* [x] Include the URL patterns from the receipts app in the expenses project with the prefix "receipts/".
* [x] Create a template for the list view that complies with the following specifications.
* [x] test

---
##feature 6
* [x] In the expenses urls.py, use the RedirectView to redirect from "" to the name of the path for the list view that you created in the previous feature. Register that path a name of "home".
* [x] test
---
##feature 7
* [x] Register the LoginView in your accounts urls.py with the path "login/" and the name "login".
* [x] Include the URL patterns from the accounts app in the expenses project with the prefix "accounts/".
* [x] Create a templates directory under accounts.
* [x] Create a registration directory under templates.
* [x] Create an HTML template named login.html in the registration directory.
* [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below).
* [x] In the expenses settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home", which will redirect us to the path (not yet created) with the name "home".
* [x] test
---
##feature 8
* [x] Protect the list view for the Receipt model so that only a person who has logged in can access it.
* [ ] Change the queryset of the view to filter the Receipt objects where purchaser equals the logged in user.
---
##feature 9
* [x] In the accounts/urls.py file: 
  * [x] Import the LogoutView from the same module that you imported the LoginView from.
  * [x] Register that view in your urlpatterns list with the path "logout/" and the name "logout".
* [x] In the expenses settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login", which will redirect the logout view to the login page.
---
##feature 10
* [x] You'll need to import the UserCreationForm from the built-in auth forms 
* [x] You'll need to use the special create_user method to create a new user account from their username and password
* [x] You'll need to use the login function that logs an account in
* [x] After you have created the user, redirect the browser to the path registered with the name "home"
* [x] Create an HTML template named signup.html in the registration directory
* [x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] test
---
##feature 11
* [x] Create a create view for the Receipt model that will show the vendor, total, tax, date, category and account properties in the form and handle the form submission to create a new Receipt
* [x] A person must be logged in to see the view
* [x] Register that view for the path "create/" in the receipts urls.py and the name "create_receipt"
* [x] Create an HTML template that shows the form to create a new Receipt (see the template specifications below)'
* [x] test 
---
##feature 12
* [x] create list view for categories
  * [x] update urls
  * [ ] create template
* [ ] create list view for account
  * [ ] update urls
  * [ ] create template
* [ ] test
---
##feature 13
* [ ]
---
##feature 14
* [ ]
---
##feature 15
* [ ]
