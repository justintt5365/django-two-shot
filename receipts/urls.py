from django.urls import path

from receipts.views import (
    ReceiptView,
    ReceiptCreate,
    ExpenseCategoryView,
    AccountView,
    ExpenseCreate,
    AccountCreate,
)

urlpatterns = [
    path("", ReceiptView.as_view(), name="home"),
    path("create/", ReceiptCreate.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryView.as_view(), name="category_view"),
    path("accounts/", AccountView.as_view(), name="account_view"),
    path(
        "categories/create/", ExpenseCreate.as_view(), name="create_category"
    ),
    path("accounts/create/", AccountCreate.as_view(), name="create_account"),
]
