from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ReceiptView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/receipt_view.html"


class ReceiptCreate(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/receipt_create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class ExpenseCategoryView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expenses/expense_view.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/account_view.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCreate(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expenses/expense_create.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("category_view")


class AccountCreate(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accounts/account_create.html"
    fields = [
        "name",
        "number",
    ]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_view")
